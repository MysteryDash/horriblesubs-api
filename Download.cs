﻿// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.0.
// The full license text can be found in the file named LICENSE.
//

namespace HorribleSubs
{
    public class Download
    {
        public string Provider { get; }
        public string Url { get; }

        public Download(string provider, string url)
        {
            Provider = provider;
            Url = url;
        }
    }
}