﻿// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.0.
// The full license text can be found in the file named LICENSE.
//

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace HorribleSubs
{
    /// <summary>
    /// The <see cref="HorribleSubsClient"/> provides methods to recover data from horriblesubs.info.
    /// </summary>
    public static class HorribleSubsClient
    {
        private static readonly HttpClient _httpClient = new HttpClient();

        /// <summary>
        /// Download the last 20 (or less) releases' informations of HorribleSubs
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an async method.")]
        public static async Task<ReadOnlyCollection<Release>> GetLastReleasesAsync()
        {
            return await GetLastReleasesAsync(0);
        }

        /// <summary>
        /// Download the last 20 (or less) releases' informations of HorribleSubs
        /// </summary>
        /// <param name="offset">Skip the specified amount of releases</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an async method.")]
        public static async Task<ReadOnlyCollection<Release>> GetLastReleasesAsync(int offset)
        {
            Contract.Requires<ArgumentException>(offset >= 0, nameof(offset));

            var httpResponse = (await _httpClient.GetAsync("http://horriblesubs.info/lib/latest.php?nextid=" + (offset / 20f).ToString(CultureInfo.InvariantCulture))).EnsureSuccessStatusCode();

            return ParseReleases(await httpResponse.Content.ReadAsStringAsync());
        }


        /// <summary>
        /// Search for the last 20 (or less) releases' informations of a specified anime on HorribleSubs
        /// </summary>
        /// <param name="animeName">The anime you are looking for</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an async method.")]
        public static async Task<ReadOnlyCollection<Release>> SearchForAsync(string animeName)
        {
            return await SearchForAsync(animeName, 0);
        }

        /// <summary>
        /// Search for the last 20 (or less) releases' informations of a specified anime on HorribleSubs
        /// </summary>
        /// <param name="animeName">The anime you are looking for</param>
        /// <param name="offset">Skip the specified amount of releases</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an async method.")]
        public static async Task<ReadOnlyCollection<Release>> SearchForAsync(string animeName, int offset)
        {
            Contract.Requires<ArgumentException>(!string.IsNullOrEmpty(animeName) || !string.IsNullOrWhiteSpace(animeName), nameof(animeName));
            Contract.Requires<ArgumentException>(offset >= 0, nameof(offset));

            var httpResponse = (await _httpClient.GetAsync("http://horriblesubs.info/lib/search.php?value=" + HttpUtility.UrlEncode(animeName) + "&nextid=" + (offset / 20f).ToString(CultureInfo.InvariantCulture))).EnsureSuccessStatusCode();

            return ParseReleases(await httpResponse.Content.ReadAsStringAsync());
        }

        /// <summary>
        /// Download the last 20 (or less) batches' informations of HorribleSubs
        /// </summary>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an async method.")]
        public static async Task<ReadOnlyCollection<Release>> GetLastBatchesAsync()
        {
            return await GetLastBatchesAsync(0);
        }

        /// <summary>
        /// Download the last 20 (or less) batches' informations of HorribleSubs
        /// </summary>
        /// <param name="offset">Skip the specified amount of batches</param>
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an async method.")]
        public static async Task<ReadOnlyCollection<Release>> GetLastBatchesAsync(int offset)
        {
            Contract.Requires<ArgumentException>(offset >= 0, nameof(offset));

            var httpResponse = (await _httpClient.GetAsync("http://horriblesubs.info/lib/latest.php?mode=batch&nextid=" + (offset / 20f).ToString(CultureInfo.InvariantCulture))).EnsureSuccessStatusCode();

            return ParseReleases(await httpResponse.Content.ReadAsStringAsync());
        }
        
        /// <summary>
        /// Parse the response received from the api into a <see cref="ReadOnlyCollection{Release}"/>
        /// </summary>
        /// <param name="data">The content of the response received from the api.</param>
        private static ReadOnlyCollection<Release> ParseReleases(string data)
        {
            var releases = new List<Release>();

            foreach (Match releaseData in Regex.Matches(data, @"release-info(?<Content>.+?(?=release-info)|.+)"))
            {
                var releaseContent = releaseData.Groups["Content"].Value;
                var episodeInformations = Regex.Match(releaseContent, @"\((?<Month>\d+)\/(?<Day>\d+)\).+?(?:(?:<.+?(?:>))|(?=\w))(?<Name>.+?(?= - \d+|<)).+?(?=\d+)(?<Number>\d+)(-(?<SecondNumber>\d+)){0,1}");

                var resolutions = (from Match resolutionData in Regex.Matches(releaseContent, @"\[(?<Resolution>\d+p)(?<Content>.+?(?=<\/div>))")
                                   let downloads = (from Match downloadData
                                                    in Regex.Matches(resolutionData.Groups["Content"].Value, @"""dl-type.+?(?=href=\"")href=\""(?<Link>.+?(?=\""))"">(?<Provider>.+?(?=<\/a>))")
                                                    select new Download(downloadData.Groups["Provider"].Value, downloadData.Groups["Link"].Value)).ToList()
                                   select new Resolution(resolutionData.Groups["Resolution"].Value, downloads)).ToList();
                
                releases.Add(new Release(episodeInformations.Groups["Name"].Value,
                                            Convert.ToInt32(episodeInformations.Groups["Day"].Value, CultureInfo.InvariantCulture),
                                            Convert.ToInt32(episodeInformations.Groups["Month"].Value, CultureInfo.InvariantCulture),
                                            Convert.ToInt32(episodeInformations.Groups["Number"].Value, CultureInfo.InvariantCulture),
                                            episodeInformations.Groups["SecondNumber"].Success ? Convert.ToInt32(episodeInformations.Groups["SecondNumber"].Value, CultureInfo.InvariantCulture) : (int?)null,
                                            episodeInformations.Groups["SecondNumber"].Success ? ReleaseType.Batch : ReleaseType.Episode,
                                            resolutions));
            }

            return new ReadOnlyCollection<Release>(releases);
        } 
    }
}