﻿// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.0.
// The full license text can be found in the file named LICENSE.
//

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HorribleSubs
{
    public class Resolution
    {
        public string Name { get; }
        public ReadOnlyCollection<Download> Downloads { get; }

        public Resolution(string name, IList<Download> downloads)
        {
            Name = name;
            Downloads = new ReadOnlyCollection<Download>(downloads);
        }
    }
}