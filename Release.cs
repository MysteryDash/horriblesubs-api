﻿// 
// This file is licensed under the terms of the Simple Non Code License (SNCL) 2.0.0.
// The full license text can be found in the file named LICENSE.
//

using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace HorribleSubs
{
    public class Release
    {
        public string Name { get; }
        public int Day { get; }
        public int Month { get; }
        public ReleaseType ReleaseType { get; }
        public int Number { get; }
        public int? EndNumber { get; }
        public ReadOnlyCollection<Resolution> Resolutions { get; }

        public Release(string name, int day, int month, int number, int? endNumber, ReleaseType releaseType, IList<Resolution> resolutions)
        {
            Name = name;
            Day = day;
            Month = month;
            ReleaseType = releaseType;
            Number = number;
            EndNumber = endNumber;
            Resolutions = new ReadOnlyCollection<Resolution>(resolutions);
        }
    }

    public enum ReleaseType
    {
        Episode,
        Batch
    }
}
