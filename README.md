# HorribleSubs .NET Api

This project is a small .NET Api designed to recover informations from the website [HorribleSubs.info](http://horriblesubs.info).

### Minimum Requirements

* .NET Framework 4.5
* Nothing More ! If you are contributing to this project, please keep the requirements unchanged.

### Version

The actual version is the 2.2

## License

This project is licensed under the terms of the SNCL (Simple Non Code License) 2.0.0.
A copy of the license is present and named LICENSE.

### Contribute to this project

If you want to contribute to this project, you just have to :
* Fork the repository and change whatever you want
* Update the version respecting this : [Major].[Minor]
* Add yourself to the list of contributors 
* Create a pull request in this repository

### Contributors

- [MysteryDash](https://bitbucket.org/MysteryDash/)